﻿using System;
using System.Xml.Linq;

namespace StudentAdvising
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string enterStudent = "Y";
            string name;
            string majorCode;
            string classification;

            do
            {
                GetData(out name, out majorCode, out classification);
                GetAdvisingLocation(name, majorCode, classification);
                Console.WriteLine("Do you want to enter another? Enter Y/N:");
                try
                {
                    enterStudent = Console.ReadLine().ToUpper();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid Name Input. Error:" + e);
                    return;
                }
                if (enterStudent != "N" && enterStudent !="Y")
                {
                    Console.WriteLine("Enter Y or N");
                    enterStudent = Console.ReadLine().ToUpper();
                }
                if (enterStudent == "N") {
                    break;
                }
            } while (enterStudent == "Y");
        }

        static void GetAdvisingLocation(string name, string majorCode, string classification) {

            string location = "";
            string major = "";

            switch (majorCode.ToUpper())

            {
                case "BIOL":
                    location = (classification.ToUpper() == "FRESHMAN" || classification.ToUpper() == "SOPHMORE") ? "Science Building, Rm 110" : "Science Building, Rm 310";
                    major = "Biology";
                    break;
                case "CSCI":
                    location = "Sheppard Hall, Room 314";
                    major = "Computer Science";
                    break;
                case "ENG":
                    location = (classification.ToUpper() == "FRESHMAN") ? "Kerr Hall, Rm 201" : "Kerr Hall, Rm 312";
                    major = "English";
                    break;
                case "HIST":
                    location = "Kerr Hall, Room 314";
                    major = "History";
                    break;
                case "MKT":
                    location = (classification.ToUpper() == "SENIOR") ? "Westly Hall, Rm 313" : "Westly Hall, Rm 310";
                    major = "Marketing";
                    break;
                default:
                    Console.WriteLine("**Error: unrecognized major code");
                    break;
            }
            /*            Display location, Ex:  Advising for English Sophomore majors: Kerr Hall, Room 312*/
            Console.WriteLine($"Hi {name}! Advising for {major} {classification} majors is in: {location}");
        }

        static void GetData(out string name, out string major, out string classification)
            {
            name = "";
            major = "";
            classification = ""; 

            Console.WriteLine("Enter Student Name: ");
            try
            {
                name = Console.ReadLine();
                try
                {
                    Console.WriteLine("Enter Student Major Code: ");
                    major = Console.ReadLine();
                    try
                    {
                        Console.WriteLine("Enter Student Classification: ");
                        classification = Console.ReadLine();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Invalid classification Input. Error: " + e);
/*                        return;*/
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid Major Input. Error: " + e);
/*                    return;*/
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Name Input. Error:" + e);
/*                return;*/
            }
        }
    }
}